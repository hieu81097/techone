<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher; // Add this line

use Cake\ORM\Entity;

class Admins extends Entity
{
    
    protected $_accessible = [
        'id'=>true,
        'username' => true,
        'password' => true,
        'name' => true,
        'phone' => true,
        'role' => true,
        'email' => true,
        'address' => true,
        'photo' => true,
        'photo_dir' => true,
        
    ];

  
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher.hash($value);
    }
}
