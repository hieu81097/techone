<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher; // Add this line

use Cake\ORM\Entity;

class Customers extends Entity
{
    
    protected $_accessible = [
        
    ];

  
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher.hash($value);
    }
}
