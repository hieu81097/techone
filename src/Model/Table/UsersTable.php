<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Behavior\TimestampBehavior;

class UsersTable extends Table{
	public function initialize(array $config)
	{
		$this->setTable('users');
		$this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                    'updated' => 'always'
                ],
                
            ]
        ]);
	}


}
?>	