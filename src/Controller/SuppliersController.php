<?php 
namespace App\Controller;
use App\Controller\AppController;

class SuppliersController extends AppController
{
	public function initialize(){
		parent::initialize();
		$this->viewBuilder()->setlayout('AdminsLayout');
		$this->loadComponent('Auth', [
            'Authenticate' => [
                'Form' => [
                	
                    'Fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
	}

	public function index(){
		$supplier = $this->Suppliers->find('all');
		$this->set('suppliers',$supplier);
		$this->set('users',$this->Auth->user());

		
	}
	public function add(){
		$supplier = $this->Suppliers->newEntity();
		if ($this->request->is('post')) {
			$supplier = $this->Suppliers->patchEntity($supplier, $this->request->getData());
			if ($this->Suppliers->save($supplier)) {
				$this->Flash->success(__('The supplier has been saved.'));

				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('The supplier could not be saved. Please, try again.'));
		}
		$this->set('supplier',$supplier);
		$this->set('users',$this->Auth->user());

	}
	public function update($id=null){
		$supplier =$this->Suppliers->get($id);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$supplier = $this->Suppliers->patchEntity($supplier, $this->request->getData());
			if ($this->Suppliers->save($supplier)) {
				$this->Flash->success(__('The supplier has been saved.'));

				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('The supplier could not be saved. Please, try again.'));
		}
		$this->set('supplier',$supplier);
		$this->set('users',$this->Auth->user());

	}
	public function delete($id)
	{
		
		$supplier = $this->Suppliers->get($id);

		if ($this->Suppliers->delete($supplier)) {
			$this->Flash->success(__('The supplier has been deleted'));
			return $this->redirect(['action'=>'index']);
		} else {
			$this->Flash->error(['ERORR']);
		}
	}

}	
?>