<?php 
namespace App\Controller;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\Component\CookieComponent;

class CustomersController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Cookie');
		$this->viewBuilder()->setlayout('CustomersLayout');
		$this->loadComponent('Auth', [
			'authenticate' => [
				'Form' => [
					'userModel'=>'Customers',
					'fields' => [
						'username' => 'username',
						'password' => 'password'
					]
				]
				
			],
			'loginAction' => [
				'controller' => 'Customers',
				'action' => 'login',
			],
		]);
		
		$this->Auth->allow(['index','registration','viewCategory','viewDetails','viewProduct','view']);
		

	}
	public function index()
	{
		$this->loadModel('Categories');
		$this->loadModel('Products');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('Products',$this->Products->find('all'));
		$this->set('customers',$this->Auth->user());
	}
	public function login()
	{
		$this->loadModel('Categories');
		if ($this->request->is('post')) {
			$customer = $this->Auth->identify();
			
			if ($customer) {
				$this->Auth->setUser($customer);				
				return $this->redirect(['action'=>'index']);
			}
			$this->Flash->error(__('Invalid Username or Password, Try again'));
		}
		$this->set('customers',$this->Auth->user());
		$this->set('Categories',$this->Categories->find('all'));
	}
	public function logout()
	{
		return $this->redirect($this->Auth->logout());

	}
	public function registration()
	{
		$customer = $this->Customers->newEntity();
		if ($this->request->is('post')) {
			$data_post = $this->request->getData();

			$customer_info = [];
			$hasher = new DefaultPasswordHasher();
			$customer_info['name']=$data_post['name'];
			$customer_info['email'] = $data_post['email'];
			$customer_info['phone']=$data_post['phone'];
			$customer_info['address'] = $data_post['address'];
			$customer_info['username']=$data_post['username'];
			$customer_info['password']= $hasher->hash($data_post['password']);
			$customer = $this->Customers->patchEntity($customer,$customer_info);
			if ($this->Customers->save($customer)) {
				$this->Flash->success(__('The Staff have been save'));
				return $this->redirect(['action'=>'login']);
				
			}
			$this->Flash->error(__('Please try again'));
		}
		$this->set('customer',$customer);
	}
	public function contact()
	{
		$this->set('customers',$this->Auth->user());
	}
	public function viewProduct()
	{
		$this->loadModel('Products');
		$this->loadModel('Categories');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('Products',$this->Products->find('all'));

		$this->set('customers',$this->Auth->user());
	}
	public function viewCategory()
	{
		$this->loadModel('Categories');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('customers',$this->Auth->user());
	} 
	public function viewDetails($id)
	{	
		
		$this->loadModel('Categories');
		$this->loadModel('Products');
		$this->loadModel('Suppliers');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('Products',$this->Products->get($id));
		$this->set('Suppliers',$this->Suppliers->find('all'));
		$this->set('customers',$this->Auth->user());
		
	}

	public function view($id){
		$this->loadModel('Categories');
		$category = $this->Categories->get($id);
		$category1 = $this->Categories->find('all');
		$this->loadModel('Products');
		$product =$this->Products->find('all',['conditions'=> ['Products.category =' => $category->id]]);
		
		$this->set('Products',$product);
		$this->set('customers',$this->Auth->user());
		$this->set('category',$category);
		$this->set('Categories',$category1);
		

	}
	public function cart($id){
		$this->autoRender = false;
		$views = $this->Cookie->read('cart');
		$this->loadModel('Products');	
		$product = $this->Products->get($id);

		$a = 1;
		
		$id = $product['id'];
		$name = $product['name'];
		$amount = $a;
		$price = $product['price'];
		$photo = $product['photo'];
		$item = ['id' => $id, 'name' => $name, 'amount' => $amount,'price' => $price, 'photo' => $photo];
		
		if(empty($views)){
			$views[] = $item;
		}else{
			$a = 0;
			foreach ($views as $i => $v ) {
				if ($views[$i]['id'] == $id) {
					$views[$i]['amount'] += 1;
					$a = 1;
				}
			}
			if ($a==0) {
				$views[] = $item;
			}
		}	
		
		$this->Cookie->write('cart',$views);	

		pr($views);
		
	}
	public function checkout()
	{	
		$views = $this->Cookie->read('cart');
		$this->loadModel('cusorderdetails');
		$this->loadModel('cusorder');
		$customer = $this->Auth->user();
		$order = $this->cusorder->newEntity();
		$orderdetails = $this->cusorderdetails->newEntities($this->request->getData());
		
		if($this->request->is('post')){
			$data_post = $this->request->getData();
			$order_info = [];
			$orderdetail_info = [];
			$status = 0;
			$order_info['cus_id'] = $customer['id'];
			$order_info['comments'] = $data_post['comments'];
			$order_info['phone'] = $data_post['phone'];
			$order_info['address'] = $data_post['address'];
			$order_info['status'] = $status;
			$order_info['total'] = trim($data_post['total'],'$ ');
			
			
			$order = $this->cusorder->patchEntity($order,$order_info);
			if ($this->cusorder->save($order)) {

				$this->Flash->success(__('The Category have been save'));
			}
			$this->Flash->error(__('The Category counld not be save.Please try again'));
			$id = $this->cusorder->find()->order(['id' => 'DESC'])->first();
			foreach ($views as $i => $v) {
				$detail = [];
				$detail['order_ID'] = $id['id'];
				$detail['pro_id'] = $views[$i]['id'];
				$detail['quantity'] = $views[$i]['amount'];
				array_push($orderdetail_info,$detail);
			}
			
			$orderdetails = $this->cusorderdetails->patchEntities($orderdetails,$orderdetail_info);
			foreach ($orderdetails as $orderdetail) {
				$this->cusorderdetails->save($orderdetail); 
				
			}
			return $this->redirect(['action'=>'index']);
			
			
		}
		$this->set('order',$order);
		$this->loadModel('Categories');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('customers',$customer);
		$this->set('views',$views);
	}
	public function addorder(){
		$this->loadModel('cusorder');
		$views = $this->Cookie->read('cart');
		$order = $this->request->newEntity();
		if($this->request->is('post')){
			$data_post = $this->request->getData();
			$order = $this->cusorder->patchEntity($order,$data_post);
			if ($this->Customers->save($customer)) {
				$this->Flash->success(__('The Staff have been save'));
				return $this->redirect(['action'=>'login']);
				
			}
			$this->Flash->error(__('Please try again'));
		}
		$this->set('order',$order);
		$this->loadModel('Categories');
		$this->set('Categories',$this->Categories->find('all'));
		$this->set('customers',$this->Auth->user());
		$this->set('views',$views);
	}
}
?>