<?php 
namespace App\Controller;
use App\Controller\AppController;

/**
* 
*/
class ProductsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->viewBuilder()->setlayout('AdminsLayout');
		$this->loadComponent('Auth', [
            'Authenticate' => [
                'Form' => [
                	
                    'Fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
	}
	public function index()
	{
		$this->loadModel('Categories');
		$this->loadModel('Suppliers');
		$list_cate = $this->Categories->find('all')->toArray();
		$list_supp = $this->Suppliers->find('all')->toArray();
		$data = [];
		$data1 = [];
		foreach ($list_cate as $category) {
			$data[$category->id] = $category->name;
		}
		foreach ($list_supp as $supplier) {
			$data1[$supplier->id] = $supplier->name;
		}
		$product = $this->Products->find('all');
		$this->set('products',$product);
		$this->set('category',$data);
		$this->set('supplier',$data1);
		$this->set('users',$this->Auth->user());

	}
	public function add()
	{
		$this->loadModel('Categories');
		$this->loadModel('Suppliers');
		$list_cate = $this->Categories->find('all')->toArray();
		$list_supp = $this->Suppliers->find('all')->toArray();
		$data = [];
		$data1 = [];
		foreach ($list_cate as $category) {
			$data[$category->id] = $category->name;
		}
		foreach ($list_supp as $supplier) {
			$data1[$supplier->id] = $supplier->name;
		}
		$product = $this->Products->newEntity();
		if ($this->request->is('post')) {
			$data_post = $this->request->getData();
			$product_info = [];
			$product_info['category']=$data_post['category'];
			$product_info['supplier']=$data_post['supplier'];
			$product_info['name']=$data_post['name'];
			$product_info['quantity']=$data_post['quantity'];
			$product_info['import_price']=$data_post['import_price'];
			$product_info['price'] = $data_post['price'];
			$product_info['description'] = $data_post['description'];
			$product_info['photo'] = $data_post['photo'];
			$product_info['photo_dir'] = $data_post['photo_dir'];

			if (!empty($data_post['photo']['name'])) {

				$fileName = $data_post['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_post['photo']['tmp_name'],$uploadFile)) {
					$product_info['photo'] = $fileName;
				}
			}

			$product = $this->Products->patchEntity($product,$product_info);
			if ($this->Products->save($product)) {

				$this->Flash->success(__('The Product have been save'));
				return $this->redirect(['action'=>'index']);
				
			}
			$this->Flash->error(__('The Product counld not be save.Please try again'));
		}

		$this->set('product',$product);
		$this->set('category',$data);
		$this->set('supplier',$data1);
		$this->set('users',$this->Auth->user());

	}
	public function update($id)
	{	
		$product = $this->Products->get($id);
		
		
		$this->loadModel('Categories');
		$this->loadModel('Suppliers');
		$list_cate = $this->Categories->find('all')->toArray();
		$list_supp = $this->Suppliers->find('all')->toArray();
		$data = [];
		$data1 = [];
		foreach ($list_cate as $category) {
			$data[$category->id] = $category->name;
		}
		foreach ($list_supp as $supplier) {
			$data1[$supplier->id] = $supplier->name;
		}
		
		if ($this->request->is(['patch','post','put'])) {
			$data_post = $this->request->getData();
			$product_info = [];
			$product_info['category']=$data_post['category'];
			$product_info['supplier']=$data_post['supplier'];
			$product_info['name']=$data_post['name'];
			$product_info['quantity']=$data_post['quantity'];
			$product_info['import_price']=$data_post['import_price'];
			$product_info['price'] = $data_post['price'];
			$product_info['description'] = $data_post['description'];
			$product_info['photo'] = $data_post['photo'];
			$product_info['photo_dir'] = $data_post['photo_dir'];

			if (!empty($data_post['photo']['name'])) {

				$fileName = $data_post['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_post['photo']['tmp_name'],$uploadFile)) {
					$product_info['photo'] = $fileName;
				}
			}

			$product = $this->Products->patchEntity($product,$product_info);
			if ($this->Products->save($product)) {

				$this->Flash->success(__('The Product have been save'));
				return $this->redirect(['action'=>'index']);
				
			}
			$this->Flash->error(__('The Product counld not be save.Please try again'));
		}

		$this->set('product',$product);
		$this->set('category',$data);
		$this->set('supplier',$data1);
		$this->set('users',$this->Auth->user());
	}
	public function delete($id)
	{
		
		$product = $this->Products->get($id);

		if ($this->Products->delete($product)) {
			$this->Flash->success(__('The Product has been deleted'));
			return $this->redirect(['action'=>'index']);
		} else {
			$this->Flash->error(['ERORR']);
		}
	}
	public function view($id)
	{
		$this->loadModel('Categories');
		$this->loadModel('Suppliers');
		$list_cate = $this->Categories->find('all')->toArray();
		$list_supp = $this->Suppliers->find('all')->toArray();
		$data = [];
		$data1 = [];
		foreach ($list_cate as $category) {
			$data[$category->id] = $category->name;
		}
		foreach ($list_supp as $supplier) {
			$data1[$supplier->id] = $supplier->name;
		}
		$product = $this->Products->get($id);

		$this->set('product',$product);
		$this->set('category',$data);
		$this->set('supplier',$data1);
		$this->set('users',$this->Auth->user());
	}
}
?>