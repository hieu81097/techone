<?php 
namespace App\Controller;
use App\Controller\AppController;

/**
* 
*/
class CategoriesController extends AppController
{
	
	public function initialize(){
		parent::initialize();
		$this->viewBuilder()->setlayout('AdminsLayout');
		$this->loadComponent('Auth', [
            'Authenticate' => [
                'Form' => [
                	
                    'Fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
	}
	public function index()
	{
		$category = $this->Categories->find('all');
		$this->set('categories',$category);
		$this->set('users',$this->Auth->user());

	}
	public function add()
	{
		$category = $this->Categories->newEntity();
		if ($this->request->is('post')) {
			$data_post = $this->request->getData();
			$category_infor = [];
			$category_infor['name'] = $data_post['name'];
			$category_infor['photo']=$data_post['photo'];
			$category_infor['photo_dir']=$data_post['photo_dir'];
			$category_infor['description']=$data_post['description'];

			if (!empty($data_post['photo']['name'])) {

				$fileName = $data_post['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_post['photo']['tmp_name'],$uploadFile)) {
					$category_infor['photo'] = $fileName;
				}
			}

			$category = $this->Categories->patchEntity($category,$category_infor);
			if ($this->Categories->save($category)) {

				$this->Flash->success(__('The Category have been save'));
				return $this->redirect(['action'=>'index']);
				
			}
			$this->Flash->error(__('The Category counld not be save.Please try again'));

		}
		$this->set('category',$category);
		$this->set('users',$this->Auth->user());

	}

	public function update($id=null)
	{
		$category = $this->Categories->get($id);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$data_post = $this->request->getData();
			$category_infor = [];
			$category_infor['name'] = $data_post['name'];
			$category_infor['photo']=$data_post['photo'];
			$category_infor['photo_dir']=$data_post['photo_dir'];
			$category_infor['description']=$data_post['description'];

			if (!empty($data_post['photo']['name'])) {

				$fileName = $data_post['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_post['photo']['tmp_name'],$uploadFile)) {
					$category_infor['photo'] = $fileName;
				}
			}

			$category = $this->Categories->patchEntity($category,$category_infor);
			if ($this->Categories->save($category)) {

				$this->Flash->success(__('The Category have been save'));
				return $this->redirect(['action'=>'index']);
				
			}
			$this->Flash->error(__('The Category counld not be save.Please try again'));

		}	
		$this->set('category',$category);
		$this->set('users',$this->Auth->user());

	}
	public function view ($id=null)
	{
		$category = $this->Categories->get($id);
		$this->set('category',$category);
		$this->set('users',$this->Auth->user());

	}
	public function delete($id=null)
	{
		
		$category = $this->Categories->get($id);

		if ($this->Categories->delete($category)) {
			$this->Flash->success(__('The category  has been deleted'));
			return $this->redirect(['action'=>'index']);
		} else {
			$this->Flash->error(['ERORR']);
		}
	}
}
?>