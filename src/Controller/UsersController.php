<?php 
namespace App\Controller;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController
{
	
	public function initialize(){
		parent::initialize();
		$this->viewBuilder()->setlayout('AdminsLayout');
		$this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                	'userModel'=>'Users',
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
		
		

	}
	 
	public function index(){
		
		$this->loadModel('Role');
		$list_role = $this->Role->find('all')->toArray();
		$data = [];
		foreach ($list_role as $role) {
			$data[$role->id] = $role->role;
		}
		$admin = $this->Users->find('all');
		$this->set('Users',$admin);
		$this->set('Role',$data);
		$this->set('users',$this->Auth->user());

	}
	public function login(){
		$this->viewBuilder()->setlayout('AdminLogin');

		if ($this->request->is('post')) {
			$admin = $this->Auth->identify();
			if ($admin) {
				$this->Auth->setUser($admin);				
				return $this->redirect(['action'=>'index']);
			}
			$this->Flash->error(__('Invalid Username or Password, Try again'));
		}
		
	}
	public function logout(){
		return $this->redirect($this->Auth->logout());
	}
	public function viewStaff($id=null)
	{	
		$this->loadModel('Role');
		$list_role = $this->Role->find('all')->toArray();
		$data = [];
		foreach ($list_role as $role) {
			$data[$role->id] = $role->role;
		}
		$admin = $this->Users->get($id);
		
		
		$this->set('user', $admin);
		$this->set('Role', $data);
		$this->set('users',$this->Auth->user());

		
	}
	public function addstaff()
	{	
		$this->loadModel('Role');
		$list_role = $this ->Role->find('all')->toArray();
		$data=[];
		foreach ($list_role as $role) {	
			$data[$role->id] = $role->role;	
		}
		$admin = $this->Users->newEntity();

		if ($this->request->is('post')) {
			$data_post = $this->request->getData();

			$admin_info = [];
			$hasher = new DefaultPasswordHasher();
			$admin_info['username']=$data_post['username'];
			$admin_info['password']= $hasher->hash($data_post['password']);
			$admin_info['name']=$data_post['name'];
			$admin_info['phone']=$data_post['phone'];
			$admin_info['role']=$data_post['role'];
			$admin_info['email'] = $data_post['email'];
			$admin_info['address'] = $data_post['address'];
			$admin_info['photo'] = $data_post['photo'];
			$admin_info['photo_dir'] = $data_post['photo_dir'];

			if (!empty($data_post['photo']['name'])) {

				$fileName = $data_post['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_post['photo']['tmp_name'],$uploadFile)) {
					$admin_info['photo'] = $fileName;
				}
			}

			$admin = $this->Users->patchEntity($admin,$admin_info);
			if ($this->Users->save($admin)) {

				$this->Flash->success(__('The Staff have been save'));
				return $this->redirect(['action'=>'index']);
				
			}
			$this->Flash->error(__('The Staff counld not be save.Please try again'));
		}
		$this->set('user',$admin);
		$this->set('Role',$data);
		$this->set('users',$this->Auth->user());
	}

	public function deleteStaff($id)
	{
		// $this->request->allowMethod(['post','delete']);
		$admin = $this->Users->get($id);

		if ($this->Users->delete($admin)) {
			$this->Flash->success(__('The Staff has been deleted'));
			return $this->redirect(['action'=>'index']);
		} else {
			$this->Flash->error(['ERORR']);
		}
	}
	public function updateStaff($id=null){
		$admin = $this->Users->get($id);
		$this->loadModel('Role');
		$list_role = $this ->Role->find('all')->toArray();
		$data=[];
		foreach ($list_role as $role) {	
			$data[$role->id] = $role->role;	
		}
		if ($this->request->is(['patch','post','put'])) {
			$data_update = $this->request->getData();

			$data = [];
			
			$data['username']=$data_update['username'];
			
			$data['name']=$data_update['name'];
			$data['phone']=$data_update['phone'];
			$data['role']=$data_update['role'];
			$data['email'] = $data_update['email'];
			$data['address'] = $data_update['address'];
			$data['photo'] = $data_update['photo'];
			$data['photo_dir'] = $data_update['photo_dir'];
			
			if (!empty($data_update['photo']['name'])) {

				$fileName = $data_update['photo']['name'];
				$uploadPath='img/';
				$uploadFile=$uploadPath.$fileName;
				
				if (move_uploaded_file($data_update['photo']['tmp_name'],$uploadFile)) {
					$data['photo'] = $fileName;
				}
			}
			$admin = $this->Users->patchEntity($admin,$data);
			if ($this->Users->save($admin)) {
				$this->Flash->success(__('The Staff have been save'));
				return $this->redirect(['action'=>'index']);
			} else {
				$this->Flash->error(__('The Staff counld not be save.Please try again'));
			}
			
		}
		$this->set('user',$admin);
		$this->set('Role',$data);
		$this->set('users',$this->Auth->user());
	}
}
?>