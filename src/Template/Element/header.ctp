<?php  ?>
<header class="header-area">
    <div class="classy-nav-container breakpoint-off">
        <div class="container">
            <!-- Classy Menu -->
            <nav class="classy-navbar justify-content-between" id="conferNav">

                <!-- Logo -->
                <a class="nav-brand" href="./index.html"><img src="/img/core-img/logo.png" alt=""></a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">
                    <!-- Menu Close Button -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav">
                            <li class="active"><a href="http://techone.web/">Home</a></li>
                            <li><a href="http://techone.web/customers/view_Category">Category</a>
                                <ul class="dropdown">
                                    <?php 
                                    foreach ($Categories as $category ) {
                                        ?>
                                        <li><a href="http://techone.web/customers/view/<?php echo $category['id'] ?>">- <?php echo $category['name'] ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li><a href="http://techone.web/customers/view_Product">Products</a></li>
                            <?php if ($customers) {
                                ?>
                                <li><a href="#"><?php echo $customers['name'] ?></a>
                                    <ul class="dropdown">
                                        <li><a href="http://techone.web/customers/logout">Log out</a></li>
                                        <li><a href="about.html">Information </a></li>
                                        <li><a href="about.html">Cart </a></li>

                                    </ul>
                                </li>
                                <li class="toyscart toyscart2 cart cart box_1">

                                    <form action="#" method="post" class="last">
                                     <input type="hidden" name="cmd" value="_cart">
                                     <input type="hidden" name="display" value="1">
                                     <button class="top_toys_cart" type="submit" name="submit" value="">
                                        <a href="http://techone.web/customers/checkout" class="btn confer-btn mt-3 mt-lg-0 ml-3 ml-lg-5">Shopping Cart
                                         <span class="fas fa-cart-arrow-down"></span></a>
                                     </button>
                                 </form>


                             </li>

                         </ul>

                         <?php 
                     } else {
                        ?>
                        <li><a href="#">Customers</a>
                            <ul class="dropdown">
                                <li><a href="http://techone.web/customers/login">Login</a></li>
                                <li><a href="http://techone.web/customers/registration">Registration </a></li>

                            </ul>
                        </li>
                    </ul>

                    <?php
                } ?>

                <!-- Get Tickets Button -->

            </div>
            <!-- Nav End -->
        </div>
    </nav>
</div>
</div>
</header>