
<div class="card">
	<div class="card-body">

		<div class="row">
			<div class="col-lg-4 single-right-left ">
				<div class="grid images_3_of_2">
					<div class="flexslider1">
						<ul class="slides">
							<li data-thumb="<?php echo $product->photo; ?>">
								<div class="thumb-image"> <?php echo $this->Html->image($product->photo,['data-imagezoom'=>'true']);?>
								</div>
							</li>

						</ul>
						<div class="clearfix"></div>
					</div>
				</div>   
			</div>
			<div class="col-lg-8 single-right-left simpleCart_shelfItem">
				<h3><?php echo $product->name; ?></h3>
				<p><span class="item_price" style="color: #DA33D8;">$ <?php echo $product->import_price; ?></span>
					<del style="color: #DA33D8;">$ <?php echo $product->price; ?></del>
				</p>
				<div class="rating1">
					<ul class="stars" >
						<li><a href="#"><i class="fa fa-star" aria-hidden="true" style="color: #DA33D8;"></i></a></li>
						<li><a href="#"><i class="fa fa-star" aria-hidden="true" style="color: #DA33D8;"></i></a></li>
						<li><a href="#"><i class="fa fa-star" aria-hidden="true" style="color: #DA33D8;"></i></a></li>
					</ul>
				</div>
				<div class="description" style="color:#DA33D8; ">
					<?php echo $product->description; ?>
				</div>
				
				<div class="description" style="color:#DA33D8;">
					<?php echo $product->quantity; ?>
				</div>
				<div class="description" style="color:#DA33D8;">
					<?php echo $category[$product->category]; ?>
				</div>
				<div class="description" style="color:#DA33D8;">
					<?php echo $supplier[$product->supplier]; ?>
				</div>


<!-- <div class="color-quality">
<div class="color-quality-right">
<h5 style="color: #DA33D8;">Size :</h5>
<select id="country1" onchange="change_country(this.value)" class="frm-field required sect">
<option value="null">2 Feet</option>
<option value="null">3 Feet</option>
<option value="null">4 Feet</option>
<option value="null">5 Feet</option>
</select>
</div>
</div> -->
<div class="occasional">

</div>
<div class="occasion-cart">
	<div class="toys single-item singlepage">
		<form action="#" method="post">
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1">
			<input type="hidden" name="toys_item" value="Farenheit">
			<input type="hidden" name="amount" value="575.00">
			<button type="submit" class=" btn btn-success" style="color: #DA33D8;">
				<i class="fa fa-edit"></i>
				<?= $this->Html->link(_('Edit'),['action'=> 'update',$product->id]) ?>
			</button>
		</button>
	</form>
</div>
</div>
<ul class="footer-social text-left mt-lg-4 mt-3">
	<li style="color: #DA33D8;">Contact : </li>
	<li class="mx-1">
		<a href="#" style="color:#DA33D8;">
			<span class="fab fa-facebook-f"></span>
		</a>
	</li>
	<li class="">
		<a href="#" style="color:#DA33D8;">
			<span class="fab fa-twitter" ></span>
		</a>
	</li>
	<li class="mx-1">
		<a href="#" style="color:#DA33D8;">
			<span class="fab fa-google-plus-g"></span>
		</a>
	</li>
	<li class="">
		<a href="#" style="color:#DA33D8;">
			<span class="fab fa-linkedin-in"></span>
		</a>
	</li>
	<li class="mx-1">
		<a href="#" style="color:#DA33D8;">
			<span class="fas fa-rss"></span>
		</a>
	</li>
</ul>
</div>
<div class="clearfix"> </div>
<!--/tabs-->
<div class="responsive_tabs">
	<div id="horizontalTab">
		<ul class="resp-tabs-list">
			<li style="color: #DA33D8;">Description</li>
			<li style="color: #DA33D8;">Reviews</li>
			<li style="color: #DA33D8;">Information</li>
		</ul>
		<div class="resp-tabs-container">
			<!--/tab_one-->
			<div class="tab1">
				<div class="single_page">
					<h6 style="color: #DA33D8;">Lorem ipsum dolor sit amet</h6>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie
						blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore
						magna aliqua.
					</p>
					<p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie
						blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore
						magna aliqua.
					</p>
				</div>
			</div>
			<!--//tab_one-->
			<div class="tab2">
				<div class="single_page">
					<div class="bootstrap-tab-text-grids">
						<div class="bootstrap-tab-text-grid">
							<div class="bootstrap-tab-text-grid-left">
								<img src="/images/team1.jpg" alt=" " class="img-fluid">
							</div>
							<div class="bootstrap-tab-text-grid-right">
								<ul>
									<li><a href="#" style="color: #DA33D8;">Admin</a></li>
									<li><a href="#" style="color: #DA33D8;"><i class="fa fa-reply-all" aria-hidden="true" style="color: #DA33D8;"></i> Reply</a></li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget.Ut enim ad minima veniam,
									quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis
									autem vel eum iure reprehenderit.
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="add-review">
							<h4 style="color: #DA33D8;">add a review</h4>
							<form action="#" method="post">
								<div class="row">
									<div class="col-md-6">
										<input type="text" name="Name" value="Name" required="">
									</div>
									<div class="col-md-6">
										<input type="email" name="Email" required="" value="Email">
									</div>
								</div>
								<textarea name="Message" required="" ></textarea>
								<input type="submit" value="SEND" >
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="tab3">
				<div class="single_page">
					<h6 style="color: #DA33D8;">Teddy Bear(Blue)</h6>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie
						blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore
						magna aliqua.
					</p>
					<p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie
						blandit ipsum auctor. Mauris volutpat augue dolor.Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore
						magna aliqua.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--//tabs-->
</div>
</div>
</div>
