<div class="card">
	<div class="card-body">
		<legend>List Products
			
			<button type="" class="btn btn-warning" >
				<a href="http://techone.web/products/add"><i class="fa fa-plus"></i></a>
			</button>
			
		</legend>
		
		<div class="single-table">
			<div class="table-responsive">
				<table class="table table-hover progress-table text-center">
					<thead class="text-uppercase">
						<tr>	
							<th><?= __('Photo_dir') ?></th>   
							<th><?= __('Category') ?></th>
							<th><?= __('Supplier') ?></th>			
							<th><?= __('Name') ?></th>
							<th><?= __('Quantity') ?></th>
							<th><?= __('Import_pice') ?></th> 
							<th><?= __('Price') ?></th>
							
							<th><?= __('Functions') ?></th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($products as $product): ?>
							<tr>
								<td>
									<?php echo $this->Html->image($product->photo);?>
								</td>
								<td>
									<?= $category[$product->category] ?>
								</td>
								<td>
									<?= $supplier[$product->supplier] ?>
								</td>
								<td>
									<?= $product->name ?>
								</td>
								<td>
									<?= $product->quantity ?>
								</td>
								<td>
									<p>$</p> <?= $product->import_price ?>
								</td>
								<td>
									<p>$</p> <?= $product->price ?>
								</td>
								
								<td>
									<ul class="d-flex justify-content-center">
										<li class="mr-3">
											<button type="" class="btn btn-primary" >
												<a href="http://techone.web/products/view/<?php echo $product->id ?>"><i class="fa fa-eye"></i></a>
											</button>
										</li>
										<li class="mr-3">
											<button type="" class="btn btn-rounded btn-success mb-3" >
												<a href="http://techone.web/products/update/<?php echo $product->id ?>"><i class="fa fa-edit"></i></a>

											</button>
										</li>
										<li class="mr-3" ><button type="" class="btn btn-rounded btn-danger mb-3" >
											<a href="http://techone.web/products/delete/<?php echo $product->id ?>" onclick="return confirm('Are you sure you want to delete this Staff?');"><i class="ti-trash"></i></a>

											
										</button></li>

									</ul>
								</td>
							</tr>
						<?php   endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>