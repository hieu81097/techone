
<div class="card">
    <div class="card-body">
        <?= $this->Form->create($product,['type'=>'file']) ?>
        <fieldset>
            <legend><?= __('Add Product') ?></legend>
            <div class="form-group">
                <?php echo $this->Form->control('category',
                ['options'=> $category , 'class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('supplier',
                ['options'=> $supplier , 'class' => 'form-control']); ?>
            </div>

            <div class="form-group">
                <?php echo $this->Form->control('name',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('quantity',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">

                <?php echo $this->Form->control('import_price',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('price',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('description',['class' => 'form-control']); ?>
            </div>
           
            <div class="form-group">


                <?= $this->Form->control('photo',['type'=>'file']) ?>

            </div>
            <div class="form-group">
                <?php echo $this->Form->control('photo_dir',['class' => 'form-control']); ?>

            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
        <?= $this->Form->end() ?>
    </div>
</div>