<div class="card">
	<div class="card-body">
		<legend>List Staff
			<?php if ($users['role']=="1") {
				?>
				<button type="" class="btn btn-warning" >
					<a href="http://techone.web/users/addstaff"><i class="fa fa-plus"></i></a>
				</button>
				<?php
			}
			?>
			
			
		</legend>
		
		<div class="single-table">
			<div class="table-responsive">
				<table class="table table-hover progress-table text-center">
					<thead class="text-uppercase">
						<tr>	
							<th><?= __('Name') ?></th>
							<th><?= __('UserName') ?></th>			
							<th><?= __('Phone') ?></th>
							<th><?= __('Role') ?></th>
							<th><?= __('Email') ?></th> 
							<th><?= __('Address') ?></th>
							<th><?= __('Photo_dir') ?></th>    
							<th><?= __('Functions') ?></th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($Users as $user): ?>
							<tr>
								<td>
									<?= $user->name ?>
								</td>
								<td>
									<?= $user->username ?>
								</td>
								<td>
									<?= $user->phone ?>
								</td>
								<td>
									<?= $Role[$user->role] ?>
								</td>
								<td>
									<?= $user->email ?>
								</td>
								<td>
									<?= $user->address ?>
								</td>
								<td>
									<?php echo $this->Html->image($user->photo);?>
								</td>
								<td>
									<ul class="d-flex justify-content-center">
										<li class="mr-3">
											<button type="" class="btn btn-primary" >
												<a href="http://techone.web/users/view_Staff/<?php echo $user->id ?>"><i class="fa fa-eye"></i></a>
											</button>
										</li>
										<?php if ($users['role']=="1") {
											?>
											<li class="mr-3">
												<button type="" class="btn btn-rounded btn-success mb-3" >
													<a href="http://techone.web/users/update_Staff/<?php echo $user->id ?>"><i class="fa fa-edit"></i></a>

												</button>
											</li>
											<li class="mr-3" ><button type="" class="btn btn-rounded btn-danger mb-3" >
												<a href="http://techone.web/users/deleteStaff/<?php echo $user->id ?>" onclick="return confirm('Are you sure you want to delete this Staff?');"><i class="ti-trash"></i></a>

												
											</button></li>
											<?php
										}
										?>
										

									</ul>
								</td>
							</tr>
						<?php   endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>