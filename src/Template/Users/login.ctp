
<div id="preloader">
            <div class="loader"></div>
        </div>
        <!-- preloader area end -->
        <!-- login area start -->
        <div class="login-area login-bg">
            <div class="container">
                <div class="login-box ptb--100">
                    <?= $this->Form->create() ?>
                        <div class="login-form-head">
                            <h4>Sign In</h4>
                            <p>Hello there, Sign in and start managing your Admin Template</p>
                        </div>
                        <div class="login-form-body">
                            <div class="form-gp">
                                
                                <?= $this->Form->control(('username')) ?>
                                <i class="ti-email"></i>
                            </div>
                            <div class="form-gp">
                                <?= $this->Form->control(('password')) ?>
                                <i class="ti-lock"></i>
                            </div>
                            <div class="row mb-4 rmber-area">
                                <div class="col-6">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                        <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="#">Forgot Password?</a>
                                </div>
                            </div>
                            <div class="submit-btn-area">
                                
                                <?= $this->Form->button(('Login'),['id'=>'form_submit']) ?>
                            </div>
                            <div class="form-footer text-center mt-5">
                                <p class="text-muted">Don't have an account? <a href="register.html">Sign up</a></p>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>