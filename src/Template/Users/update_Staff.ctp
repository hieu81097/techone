
<div class="card">
	<div class="card-body">
		<?= $this->Form->create($user,['type'=>'file']) ?>
		<fieldset>
			<legend><?= __('Add Staff') ?></legend>
			<div class="form-group">
				<?php echo $this->Form->control('username',['class' => 'form-control']); ?>
			</div>

			<div class="form-group">

				<?php echo $this->Form->control('name',['class' => 'form-control']); ?>
			</div>
			<div class="form-group">
				<?php echo $this->Form->control('phone',['class' => 'form-control']); ?>
			</div>
			<div class="form-group">
				<?php echo $this->Form->control('role',
				['options'=> $Role , 'class' => 'form-control']); ?>
			</div>
			<div class="form-group">
				<?php echo $this->Form->control('email',['class' => 'form-control']); ?>
			</div>
			<div class="form-group">
				<?php echo $this->Form->control('address',['class' => 'form-control']); ?>
			</div>
			<div class="form-group">

				<?= $this->Form->control('photo',['type'=>'file']) ?>

			</div>
			<div class="form-group">
				<?php echo $this->Form->control('photo_dir',['class' => 'form-control']); ?>

			</div>
		</fieldset>

		<?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary mt-4 pr-4 pl-4']) ?>
		<?= $this->Form->end() ?>
	</div>
</div>	