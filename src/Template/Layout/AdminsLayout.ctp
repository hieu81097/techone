<?php
/**
 * cakephp(tm) : rapid development framework (https://cakephp.org)
 * copyright (c) cake software foundation, inc. (https://cakefoundation.org)
 *
 * licensed under the mit license
 * for full copyright and license information, please see the license.txt
 * redistributions of files must retain the above copyright notice.
 *
 * @copyright     copyright (c) cake software foundation, inc. (https://cakefoundation.org)
 * @link          https://cakephp.org cakephp(tm) project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php mit license
 */

$cakedescription = 'techone - the best thing for you';
?>
<!doctype html>
<html>
<head>

    <title>
        <?= $cakedescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/img/core-img/logo.png">    
    <link rel="stylesheet" href="/admin/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/assets/css/themify-icons.css">
    <link rel="stylesheet" href="/admin/assets/css/metismenu.css">
    <link rel="stylesheet" href="/admin/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/admin/assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="/admin/assets/css/typography.css">
    <link rel="stylesheet" href="/admin/assets/css/default-css.css">
    <link rel="stylesheet" href="/admin/assets/css/styles.css">
    <link rel="stylesheet" href="/admin/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="/admin/assets/js/vendor/modernizr-2.8.3.min.js"></script>
  <!--   <script>
       addeventlistener("load", function () {
        settimeout(hideurlbar, 0);
    }, false);

       function hideurlbar() {
        window.scrollto(0, 1);
    }
</script>
<!--//meta tags ends here-->
<!--booststrap-->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<!--//booststrap end-->
<!-- font-awesome icons -->
<link href="/css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
<!-- //font-awesome icons -->
<!--shoping cart-->
<link rel="stylesheet" href="/css/shop.css" type="text/css" />
<!--//shoping cart-->
<link rel="stylesheet" type="text/css" href="/css/jquery-ui1.css">
<link href="/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!--stylesheets-->
<link href="/css/style2.css" rel='stylesheet' type='text/css' media="all">
<!--//stylesheets-->
<link href="//fonts.googleapis.com/css?family=sunflower:500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=open+sans:400,600,700" rel="stylesheet">
</head>
<body>
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.html"><img src="/img/core-img/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <?php 
                        if ($users['role'] == "1") {
                            ?>
                            <ul class="metismenu" id="menu">
                                <li class="active">
                                    <a href="http://techone.web/users   /" aria-expanded="true"><i class="fa fa fa-users"></i><span>staff</span></a>

                                </li>
                                <li>
                                    <a href="http://techone.web/suppliers" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>supplier
                                    </span></a>

                                </li>
                                <li>
                                    <a href="http://techone.web/products" aria-expanded="true"><i class="ti-pie-chart"></i><span>product</span></a>

                                </li>

                                <li>
                                    <a href="http://techone.web/categories" aria-expanded="true"><i class="fa fa-align-left"></i> <span>category
                                    </span></a>

                                </li>
                            </ul>

                            <?php 
                        }

                        ?>
                        <?php if ($users['role'] == "2") {
                           ?>
                           <ul class="metismenu" id="menu">
                            <li class="active">
                                <a href="http://techone.web/admins/" aria-expanded="true"><i class="fa fa fa-users"></i><span>customer</span></a>

                            </li>
                            <li>
                                <a href="http://techone.web/suppliers" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>payment
                                </span></a>

                            </li>
                            <li>
                                <a href="http://techone.web/products" aria-expanded="true"><i class="ti-pie-chart"></i><span>report</span></a>

                            </li>

                        
                        </ul>
                        <?php  
                    }
                    ?>

                </nav>
            </div>
        </div>
    </div>
    <!-- sidebar menu area end -->
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="search-box pull-left">
                        <form action="#">
                            <input type="text" name="search" placeholder="search..." required>
                            <i class="ti-search"></i>
                        </form>
                    </div>
                </div>
                <!-- profile info & task notification -->
                <div class="col-md-6 col-sm-4 clearfix">
                    <ul class="notification-area pull-right">
                        <li id="full-view"><i class="ti-fullscreen"></i></li>
                        <li id="full-view-exit"><i class="ti-zoom-out"></i></li>
                        <li class="dropdown">
                            <i class="ti-bell dropdown-toggle" data-toggle="dropdown">
                                <span>2</span>
                            </i>
                            <div class="dropdown-menu bell-notify-box notify-box">
                                <span class="notify-title">you have 3 new notifications <a href="#">view all</a></span>
                                <div class="nofity-list">
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                        <div class="notify-text">
                                            <p>you have changed your password</p>
                                            <span>just now</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-comments-smiley btn-info"></i></div>
                                        <div class="notify-text">
                                            <p>new commetns on post</p>
                                            <span>30 seconds ago</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-key btn-primary"></i></div>
                                        <div class="notify-text">
                                            <p>some special like you</p>
                                            <span>just now</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-comments-smiley btn-info"></i></div>
                                        <div class="notify-text">
                                            <p>new commetns on post</p>
                                            <span>30 seconds ago</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-key btn-primary"></i></div>
                                        <div class="notify-text">
                                            <p>some special like you</p>
                                            <span>just now</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                        <div class="notify-text">
                                            <p>you have changed your password</p>
                                            <span>just now</span>
                                        </div>
                                    </a>
                                    <a href="#" class="notify-item">
                                        <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                        <div class="notify-text">
                                            <p>you have changed your password</p>
                                            <span>just now</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                        
                        <li class="settings-btn">
                            <i class="ti-settings"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">dashboard</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="index.html">home</a></li>
                            <li><span>dashboard</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">

                        <?php echo $this->html->image($users['photo'],["class"=>"avatar user-thumb","alt"=>"avatar"]);?>
                        <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?php echo $users['name']; ?><i class="fa fa-angle-down"></i></h4>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#"><i class="fa fa-info-circle"></i>    information</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-gear"></i>    settings</a>
                            <!-- <a class="dropdown-item" href="http://techone.web/users/logout"><i class="fa fa-share-square"></i>    log out</a> -->
                            <?php echo $this->html->link(__('logout'),['controller'=>'users','action'=>'logout',"class"=>"dropdown-item"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <div class="row">
                <div class="col-12 mt-5">
                    <?= $this->fetch("content") ?>
                </div>


            </div>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->

    <!-- footer area end-->
</div>



<?php echo  $this->element("footeradmin"); ?>
<script src="/admin/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap 4 js -->
<script src="/admin/assets/js/popper.min.js"></script>
<script src="/admin/assets/js/bootstrap.min.js"></script>
<script src="/admin/assets/js/owl.carousel.min.js"></script>
<script src="/admin/assets/js/metismenu.min.js"></script>
<script src="/admin/assets/js/jquery.slimscroll.min.js"></script>
<script src="/admin/assets/js/jquery.slicknav.min.js"></script>

<!-- start chart js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chart.js/2.7.2/chart.min.js"></script>
<!-- start highcharts js -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<!-- start amcharts -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/ammap.js"></script>
<script src="https://www.amcharts.com/lib/3/maps/js/worldlow.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<!-- all line chart activation -->
<script src="/admin/assets/js/line-chart.js"></script>
<!-- all pie chart -->
<script src="/admin/assets/js/pie-chart.js"></script>
<!-- all bar chart -->
<script src="/admin/assets/js/bar-chart.js"></script>
<!-- all map chart -->
<script src="/admin/assets/js/maps.js"></script>
<!-- others plugins -->
<script src="/admin/assets/js/plugins.js"></script>
<script src="/admin/assets/js/scripts.js"></script>
<!--     <script src="/js/jquery-2.2.3.min.js"></script>
    <!-- newsletter modal -->
    <!-- cart-js -->
    <script src="/js/minicart.js"></script>
    <script>
     toys.render();

     toys.cart.on('toys_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
});
</script>
<!-- //cart-js -->
<!-- price range (top products) -->
<script src="/js/jquery-ui.js"></script>
<script>
         //<![cdata[ 
         $(window).load(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 9000,
                values: [50, 6000],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });
            $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

         }); //]]>
     </script>
     <!-- //price range (top products) -->
     <!-- single -->
     <script src="/js/imagezoom.js"></script>
     <!-- single -->
     <!-- script for responsive tabs -->
     <script src="/js/easy-responsive-tabs.js"></script>
     <script>
         $(document).ready(function () {
            $('#horizontaltab').easyresponsivetabs({
                type: 'default', //types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // start closed if in accordion view
                activate: function (event) { // callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabinfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('#verticaltab').easyresponsivetabs({
                type: 'vertical',
                width: 'auto',
                fit: true
            });
        });
    </script>
    <!-- flexslider -->
    <script src="/js/jquery.flexslider.js"></script>
    <script>
         // can also be used with $(document).ready()
         $(window).load(function () {
            $('.flexslider1').flexslider({
                animation: "slide",
                controlnav: "thumbnails"
            });
        });
    </script>
    <!-- //flexslider-->
    <!-- start-smoth-scrolling -->
    <script src="/js/move-top.js"></script>
    <script src="/js/easing.js"></script>
    
    <!-- //here ends scrolling icon -->
    <!-- //smooth-scrolling-of-move-up -->
    <!--bootstrap working-->
    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
