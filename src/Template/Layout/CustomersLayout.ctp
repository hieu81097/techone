<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'TechOne - The best thing for you';
?>
<!DOCTYPE html>
<html>
<head>

    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/img/core-img/logo.png">

    <!--===============================================================================================-->
    <link rel="stylesheet" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet"  href="/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="/vendor/animate/animate.css">
    <!--===============================================================================================-->  
    <link rel="stylesheet"  href="/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet"  href="/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet"  href="/vendor/select2/select2.min.css">
    <!--===============================================================================================-->  
    <link rel="stylesheet"  href="/vendor/daterangepicker/daterangepicker.css">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="/style.css">
    <?= $this->Html->css('util')?>
    <?= $this->Html->css('main')?>
    <script>
     addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

     function hideURLbar() {
        window.scrollTo(0, 1);
    }
</script>
<!--//meta tags ends here-->
<!--booststrap-->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<!--//booststrap end-->
<!-- font-awesome icons -->
<link href="/css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
<!-- //font-awesome icons -->
<!--Shoping cart-->
<link rel="stylesheet" href="/css/shop.css" type="text/css" />
<!--//Shoping cart-->
<link rel="stylesheet" type="text/css" href="/css/jquery-ui1.css">
<link href="/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!--stylesheets-->
<link href="/css/style2.css" rel='stylesheet' type='text/css' media="all">
<link rel="stylesheet" type="text/css" href="/css/checkout.css">
<!--//stylesheets-->
<link href="//fonts.googleapis.com/css?family=Sunflower:500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

</head>
<body>
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <?php echo  $this->element("header");?>
    <?php echo $this->element("intro") ?>
    <div class="">
        <?= $this->fetch("content") ?>
    </div>
    <?php echo  $this->element("footer"); ?>

    <script src="/js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="/js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="/js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="/js/default-assets/active.js"></script>
    <script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="/vendor/bootstrap/js/popper.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="/vendor/daterangepicker/moment.min.js"></script>
    <script src="/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="/vendor/countdowntime/countdowntime.js"></script>
    <script src="/js/jquery-2.2.3.min.js"></script>
    <!-- newsletter modal -->
    <!-- cart-js -->
    <script src="/js/minicart.js"></script>
    <script>
     toys.render();

     toys.cart.on('toys_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
});
</script>
<!-- //cart-js -->
<!-- price range (top products) -->
<script src="/js/jquery-ui.js"></script>
<script>
         //<![CDATA[ 
         $(window).load(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 9000,
                values: [50, 6000],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });
            $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

         }); //]]>
     </script>
     <!-- //price range (top products) -->
     <!-- single -->
     <script src="/js/imagezoom.js"></script>
     <!-- single -->
     <!-- script for responsive tabs -->
     <script src="/js/easy-responsive-tabs.js"></script>
     <script>
         $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('#verticalTab').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true
            });
        });
    </script>
    <!-- FlexSlider -->
    <script src="/js/jquery.flexslider.js"></script>
    <script>
         // Can also be used with $(document).ready()
         $(window).load(function () {
            $('.flexslider1').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
    
    <!-- //FlexSlider-->
    <!-- start-smoth-scrolling -->
    <script src="/js/move-top.js"></script>
    <script src="/js/easing.js"></script>
    
    <!-- //here ends scrolling icon -->
    <!-- //smooth-scrolling-of-move-up -->
    <!--bootstrap working-->
    <script src="/js/bootstrap.min.js"></script>
    

</body>
</html>
