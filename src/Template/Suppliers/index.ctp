<div class="card">
	<div class="card-body">
		<legend> List Supplier

			
				<button type="" class="btn btn-warning" >
					<a href="http://techone.web/suppliers/add"><i class="fa fa-plus"></i></a>
				</button>
			
		</legend>
		
		<div class="single-table">
			<div class="table-responsive">
				<table class="table table-hover progress-table text-center">
					<thead class="text-uppercase">
						<tr>	
							<th><?= __('Name') ?></th>
							<th><?= __('Description') ?></th>			
							    
							<th><?= __('Functions') ?></th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($suppliers as $supplier): ?>
							<tr>
								<td>
									<?= $supplier->name ?>
								</td>
								<td>
									<?= $supplier->description ?>
								</td>
								
								<td>
									<ul class="d-flex justify-content-center">
										<li class="mr-3">
											<button type="" class="btn btn-primary" >
												<a href="http://techone.web/suppliers/view/<?php echo $supplier->id ?>"><i class="fa fa-eye"></i></a>
											</button>
										</li>
										<li class="mr-3">
											<button type="" class="btn btn-rounded btn-success mb-3" >
												<a href="http://techone.web/suppliers/update/<?php echo $supplier->id ?>"><i class="fa fa-edit"></i></a>

											</button>
										</li>
										<li class="mr-3" ><button type="" class="btn btn-rounded btn-danger mb-3" >
											<a href="http://techone.web/suppliers/delete/<?php echo $supplier->id ?>" onclick="return confirm('Are you sure you want to delete this Supplier?');"><i class="ti-trash"></i></a>

												
											</button></li>

										</ul>
									</td>
								</tr>
							<?php   endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>