
<div class="card">
                    <div class="card-body">
                        <?= $this->Form->create($supplier) ?>
                        <fieldset>
                            <legend><?= __('Add Supplier') ?></legend>
                            

                            <div class="form-group">
                                <?php echo $this->Form->control('name',['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->control('description',['class' => 'form-control']); ?>
                            </div>
                            
                        </fieldset>
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
                        <?= $this->Form->end() ?>
                    </div>
                </div>