<div class="card">
	<div class="card-body">
		<legend> List Category

			
			<button type="" class="btn btn-warning" >
				<a href="http://techone.web/categories/add"><i class="fa fa-plus"></i></a>
			</button>
			
		</legend>
		
		<div class="single-table">
			<div class="table-responsive">
				<table class="table table-hover progress-table text-center">
					<thead class="text-uppercase">
						<tr>	
							<th><?= __('Name') ?></th>
							<th><?= __('Description') ?></th>			
							<th><?= __('photo_dir') ?></th>
							<th><?= __('Functions') ?></th>

						</tr>
					</thead>
					<tbody>

						<?php foreach ($categories as $category): ?>
							<tr>
								<td>
									<?= $category->name ?>
								</td>
								<td>
									<?= $category->description ?>
								</td>
								<td>
									<?php echo $this->Html->image($category->photo);?>

								</td>
								
								<td>
									<ul class="d-flex justify-content-center">
										<li class="mr-3">
											<button type="" class="btn btn-primary" >
												<a href="http://techone.web/categories/view/<?php echo $category->id ?>"><i class="fa fa-eye"></i></a>
											</button>
										</li>
										<li class="mr-3">
											<button type="" class="btn btn-rounded btn-success mb-3" >
												<a href="http://techone.web/categories/update/<?php echo $category->id ?>"><i class="fa fa-edit"></i></a>

											</button>
										</li>
										<li class="mr-3" ><button type="" class="btn btn-rounded btn-danger mb-3" >
											<a href="http://techone.web/categories/delete/<?php echo $category->id ?>" onclick="return confirm('Are you sure you want to delete this Supplier?');"><i class="ti-trash"></i></a>

											<?= $this->Form->postLink(__('Delete'),['action'=>'delete',$category->id],['confirm'=>__('Are you sure you want to delete this supplier?',$category->id)]) ?>
										</button></li>

									</ul>
								</td>
							</tr>
						<?php   endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>