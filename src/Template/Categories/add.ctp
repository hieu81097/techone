
<div class="card">
    <div class="card-body">
        <?= $this->Form->create($category,['type'=>'file']) ?>
        <fieldset>
            <legend><?= __('Add Category') ?></legend>


            <div class="form-group">
                <?php echo $this->Form->control('name',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('description',['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('photo',['type'=>'file']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('photo_dir',['class' => 'form-control']); ?>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
        <?= $this->Form->end() ?>
    </div>
</div>