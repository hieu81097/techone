<section class="our-speaker-area bg-img bg-gradient-overlay section-padding-100-60" style="background-image: url(/img/bg-img/3.jpg);">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading text-center wow fadeInUp" data-wow-delay="300ms">
                    <p>Our Speakings</p>
                    <h4>Products</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Single Speaker Area -->
            <?php 
            foreach ($Products as $product) {

                ?>
                
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-speaker-area bg-gradient-overlay-2 wow fadeInUp" data-wow-delay="300ms">
                            <!-- Thumb -->
                            <div class="speaker-single-thumb">
                                <img src="/img/<?php echo $product['photo'] ?>" alt="">
                            </div>
                            <!-- Social Info -->
                            <div class="social-info">
                                <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                <a href="http://techone.web/customers/view_Details/<?php echo $product['id'] ?>"><i class="fa fa-eye"></i></a>
                            </div>
                            <!-- Info -->
                            <div class="speaker-info">
                                <h5><?php echo $product['name']; ?></h5>
                                <p><span>$</span><?php echo $product['price']; ?></p>
                                <p><?php echo $product['description']; ?></p>
                            </div>
                        </div>
                    </div>
            
                <?php
            }
            ?>

            
        </div>
    </div>
</section>