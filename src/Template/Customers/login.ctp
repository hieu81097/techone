

<!-- Scroll Icon -->
<div class="icon-scroll" id="scrollDown"></div>
</section>

<section class="about-us-countdown-area section-padding-100-0" id="about">
	<div class="container">
		<div class="row align-items-center">
			<!-- About Conten t-->
			
			<?= $this->Form->create(null,['class'=>'login100-form validate-form']) ?>
			<span class="login100-form-logo">
				<i class="zmdi zmdi-landscape"></i>
			</span>

			<span class="login100-form-title p-b-34 p-t-27">
				Log in
			</span>

			<div class="wrap-input100 validate-input" data-validate = "Enter username">
				<input class="input100" type="text" name="username" placeholder="Username">
				<span class="focus-input100" data-placeholder="&#xf207;"></span>
			</div>

			<div class="wrap-input100 validate-input" data-validate="Enter password">
				<input class="input100" type="password" name="password" placeholder="Password">
				<span class="focus-input100" data-placeholder="&#xf191;"></span>
			</div>

			<div class="contact100-form-checkbox">
				<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
				<label class="label-checkbox100" for="ckb1">
					Remember me
				</label>
			</div>

			<div class="container-login100-form-btn">
				
				<?= $this->Form->button(('Login'),['class'=>'login100-form-btn']) ?>
			</div>

			<div class="text-center p-t-90">
				<a class="txt1" href="#">
					Forgot Password?
				</a>
			</div>
			<?= $this->Form->end() ?>
			<!-- About Thumb -->

		</div>
	</div>

	<!-- Counter Up Area -->
	<div class="countdown-up-area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-3">
					<!-- Countdown Text -->
					<div class="countdown-content-text mb-100 wow fadeInUp" data-wow-delay="300ms">
						<h6>Conference Date</h6>
						<h4>Count Every Second Until the Event</h4>
					</div>
				</div>

				<div class="col-12 col-md-9">
					<div class="countdown-timer mb-100 wow fadeInUp" data-wow-delay="300ms">
						<div id="clock"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>