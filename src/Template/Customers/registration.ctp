

	<!-- Scroll Icon -->
	<div class="icon-scroll" id="scrollDown"></div>
</section>

<section class="about-us-countdown-area section-padding-100-0" id="about">
	<div class="container">
		<div class="row align-items-center">
			<!-- About Content -->
			
				<?= $this->Form->create($customer,['class'=>'login100-form validate-form']) ?>
				<span class="login100-form-logo">
					<i class="zmdi zmdi-landscape"></i>
				</span>

				<span class="login100-form-title p-b-34 p-t-27">
					Registration
				</span>

				<div class="wrap-input100 validate-input" data-validate = "Enter username">
					<input class="input100" type="text" name="username" placeholder="Username">
					<span class="focus-input100" ></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Enter password">
					<input class="input100" type="password" name="password" placeholder="Password">
					<span class="focus-input100" ></span>
				</div>	
				<div class="wrap-input100 validate-input" data-validate = "Enter fullname">
					<input class="input100" type="text" name="name" placeholder="Fullname">
					<span class="focus-input100" ></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate = "Enter phone">
					<input class="input100" type="phone" name="phone" placeholder="Phone">
					<span class="focus-input100" ></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate = "Enter email">
					<input class="input100" type="email" name="email" placeholder="Email">
					<span class="focus-input100" ></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate = "Enter address">
					<input class="input100" type="text" name="address" placeholder="Address">
					<span class="focus-input100"></i></span>
				</div>

				<div class="container-login100-form-btn">

					<?= $this->Form->button(('Registration'),['class'=>'login100-form-btn']) ?>
				</div>
 		
				<div class="text-center p-t-90">
					
				</div>
			<?= $this->Form->end() ?>
			<!-- About Thumb -->

		</div>
	</div>

	<!-- Counter Up Area -->
	
</section>