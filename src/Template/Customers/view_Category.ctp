<section class="our-blog-area bg-img bg-gradient-overlay section-padding-100-60" style="background-image: url(/img/bg-img/17.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Heading -->
                <div class="section-heading text-center wow fadeInUp" data-wow-delay="300ms">
                    <p>Our Blog</p>
                    <h4>Latest news</h4>
                </div>
            </div>
            <?php foreach ($Categories as $category) {
                ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-blog-area wow fadeInUp" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="/img/<?php echo $category['photo'] ?>" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#"><?php echo $category['name']; ?></a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> January 14, 2019</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i> Laura Green</a>
                            </div>
                            <p><?php echo $category['description']; ?></p>
                        </div>
                        <div class="blog-btn">
                            <a href="http://techone.web/customers/view/<?php echo $category['id'] ?>"><i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php
            } 
            ?>
            <!-- Single Blog Area -->


        </div>
    </div>
</section>