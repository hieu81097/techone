<section class="about-us-countdown-area section-padding-100-0" id="about">

   <div class="shop_inner_inf">
      <div class="privacy about">
         <h3 style="color: #DA33D8;">Chec<span style="color: #DA33D8;">kout</span></h3>
         <div class="checkout-right">
            <h4 style="color: #DA33D8;">Your shopping cart contains: <span style="color: #DA33D8;">3 Products</span></h4>
            <table class="timetable_sub">
               <thead>
                  <tr>
                     <th >SL No.</th>
                     <th >Product</th>
                     <th >Quality</th>
                     <th >Product Name</th>
                     <th >Price</th>
                     <th >Remove</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  foreach ($views as $i => $v) {
                     ?>
                     <tr class="<?php echo $views[$i]['id'] ?>">
                        <td class="invert"><?php echo $views[$i]['id'] ?></td>
                        <td class="invert-image"><a href="single.html"><img src="/img/<?php echo $views[$i]['photo']  ?>" alt=" " class="img-responsive"></a></td>
                        <td class="invert">
                           <div class="quantity">
                              <div class="quantity-select">
                                 <div class="entry value-minus">&nbsp;</div>
                                 <div class="entry value"><span ><?php echo $views[$i]['amount'] ?></span></div>
                                 <div class="entry value-plus active">&nbsp;</div>
                              </div>
                           </div>
                        </td>
                        <td class="invert" style="color: #DA33D8;"><?php echo $views[$i]['name'] ?></td>
                        <td class="invert" style="color: #DA33D8;">$ <?php echo $prices = $views[$i]['amount']*$views[$i]['price'].",00" ?></td>
                        <td class="invert">
                           <div class="rem">
                              <div class="close1"> </div>
                           </div>
                        </td>
                     </tr>
                     <?php 
                  }
                  ?>


               </tbody>
            </table>
         </div>
         <div class="checkout-left">
            <div class="col-md-4 checkout-left-basket">
               <h4
               style="color: #DA33D8;">Continue to basket</h4>
               <ul>
                  <?php 
                  foreach ($views as $i => $v) {
                     ?>
                     <li style="color: #DA33D8;"><?php echo $views[$i]['name'] ?><i>-</i> <span style="color: #DA33D8;">$ <?php $prices = $views[$i]['amount']*$views[$i]['price'];
                     echo $prices.",00"; ?></span></li>
                     <?php 
                  } ?>
                  
                  <li style="color: #DA33D8;">Total Service Charges <i>-</i> <span style="color: #DA33D8;">$<?php $discount = 55;
                  echo $discount.",00"; ?></span></li>
                  <li style="color: #DA33D8;">Total <i>-</i> <span style="color: #DA33D8;">
                     $ <?php 
                     $Total = 0;
                     foreach ($views as $i => $v) {
                        $prices = $views[$i]['amount']*$views[$i]['price'];
                        $Total += $prices;
                     }
                     $TotalP = $Total - $discount;
                     echo $TotalP.",00";
                     ?>


                  </span></li>
               </ul>
            </div>
            <div class="col-md-8 address_form">
               <h4 style="color: #DA33D8;">Add a new Details</h4>
               <?= $this->Form->create() ?>
               <section class="creditly-wrapper wrapper">
                  <div class="information-wrapper">
                     <div class="first-row form-group">
                        <div class="controls">
                           <label class="control-label" style="color: #DA33D8;">Full name: </label>
                           <input class="billing-address-name form-control" type="text" name="name" placeholder="Full name" value="<?php echo $customers['name'] ?>">
                        </div>
                        <div class="card_number_grids">
                           <div class="card_number_grid_left">
                              <div class="controls">
                                 <label class="control-label" style="color: #DA33D8;">Mobile number:</label>
                                 <input class="form-control" type="text" name="phone" placeholder="Mobile number" value="<?php echo $customers['phone'] ?>">
                              </div>
                           </div>
                           <div class="card_number_grid_right">
                              <div class="controls">
                                 <label class="control-label" style="color: #DA33D8;">Landmark: </label>
                                 <input class="form-control" type="text" placeholder="Landmark" name="address" value="<?php echo $customers['address'] ?>">
                              </div>
                           </div>
                           <div class="clear"> </div>
                        </div>
                        <div class="controls">
                           <label class="control-label" style="color: #DA33D8;">Comment: </label>

                           <textarea class="form-control" type="text" name="comments" placeholder="Comment" ></textarea>
                        </div>
                        <div class="controls" >
                           <label class="control-label" style="color: #DA33D8;">Total: </label>
                           <input class="form-control" type="text" placeholder="Landmark" name="total" value=" $ <?php 
                           $Total = 0;
                           foreach ($views as $i => $v) {
                              $prices = $views[$i]['amount']*$views[$i]['price'];
                              $Total += $prices;
                           }
                           $TotalP = $Total - $discount;
                           echo $TotalP;
                           ?>" readonly>

                        </div>


                     </div>
                     <div class="checkout-right-basket">
                        <?= $this->Form->button(__('Payment'),['class'=>'submit check_out']) ?>
                     </div>
                  </div>
               </section>
               <?= $this->Form->end() ?>

            </div>
            <div class="clearfix"> </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
   <!-- //top products -->
</section>
<!--js working-->
<script src='/js/jquery-2.2.3.min.js'></script>
<!--//js working-->
<!-- cart-js -->  
<script src="/js/minicart.js"></script>
<script>
   toys.render();

   toys.cart.on('toys_checkout', function (evt) {
      var items, len, i;

      if (this.subtotal() > 0) {
         items = this.items();
         
         for (i = 0, len = items.length; i < len; i++) {}
      }
});
</script>
<!--// cart-js -->
<!--quantity-->
<script>
   $('.value-plus').on('click', function () {
      var divUpd = $(this).parent().find('.value'),
      newVal = parseInt(divUpd.text(), 10) + 1;
      divUpd.text(newVal);
   });

   $('.value-minus').on('click', function () {
      var divUpd = $(this).parent().find('.value'),
      newVal = parseInt(divUpd.text(), 10) - 1;
      if (newVal >= 1) divUpd.text(newVal);
   });
</script>
<!--quantity-->
<!--closed-->
<script>
   $(document).ready(function (c) {
      $('.close1').on('click', function (c) {
         $('.<?php echo $views[$i]['id'] ?>').fadeOut('slow', function (c) {
            $('.<?php echo $views[$i]['id'] ?>').remove();
         });
      });
   });
</script>
<script>
   $(document).ready(function (c) {
      $('.close2').on('click', function (c) {
         $('.rem2').fadeOut('slow', function (c) {
            $('.rem2').remove();
         });
      });
   });
</script>
<script>
   $(document).ready(function (c) { 
      $('.close3').on('click', function (c) {
         $('.rem3').fadeOut('slow', function (c) {
            $('.rem3').remove();
         });
      });
   });
</script>
<!--//closed-->
<!-- start-smoth-scrolling -->
<script src="/js/move-top.js"></script>
<script src="/js/easing.js"></script>

<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->

<!-- //here ends scrolling icon -->
<!--bootstrap working-->
<script src="/js/bootstrap.min.js"></script>